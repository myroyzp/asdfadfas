-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: curso_online
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id_admin` int NOT NULL AUTO_INCREMENT,
  `lenguaje_pro` varchar(100) NOT NULL,
  `id_empleado_fk` int NOT NULL,
  PRIMARY KEY (`id_admin`),
  KEY `fk_admin_empleado1_idx` (`id_empleado_fk`),
  CONSTRAINT `fk_admin_empleado1` FOREIGN KEY (`id_empleado_fk`) REFERENCES `empleado` (`id_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumno` (
  `id_alumno` int NOT NULL AUTO_INCREMENT,
  `cuenta_al` varchar(45) NOT NULL,
  `id_tipo_alumno_fk` int NOT NULL,
  PRIMARY KEY (`id_alumno`),
  KEY `fk_alumno_tipo_alumno1_idx` (`id_tipo_alumno_fk`),
  CONSTRAINT `fk_alumno_tipo_alumno1` FOREIGN KEY (`id_tipo_alumno_fk`) REFERENCES `tipo_alumno` (`id_tipo_alumno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudad` (
  `id_ciudad` int NOT NULL AUTO_INCREMENT,
  `nombre_ciudad` varchar(100) NOT NULL,
  `id_pais_fk` int NOT NULL,
  PRIMARY KEY (`id_ciudad`),
  KEY `fk_ciudad_pais1_idx` (`id_pais_fk`),
  CONSTRAINT `fk_ciudad_pais1` FOREIGN KEY (`id_pais_fk`) REFERENCES `pais` (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'cdmx',1),(106,'los macakos',3),(107,'buenos aires',2);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colonia`
--

DROP TABLE IF EXISTS `colonia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `colonia` (
  `id_colonia` int NOT NULL AUTO_INCREMENT,
  `nombre_colonia` varchar(100) NOT NULL,
  `id_municipio_fk` int NOT NULL,
  PRIMARY KEY (`id_colonia`),
  KEY `fk_colonia_municipio1_idx` (`id_municipio_fk`),
  CONSTRAINT `fk_colonia_municipio1` FOREIGN KEY (`id_municipio_fk`) REFERENCES `municipio` (`id_municipio`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colonia`
--

LOCK TABLES `colonia` WRITE;
/*!40000 ALTER TABLE `colonia` DISABLE KEYS */;
/*!40000 ALTER TABLE `colonia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comprobante_pago`
--

DROP TABLE IF EXISTS `comprobante_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comprobante_pago` (
  `id_comprobante_pago` int NOT NULL AUTO_INCREMENT,
  `comprobante_pagocol` varchar(45) NOT NULL,
  `id_suscripcion_fk` int NOT NULL,
  PRIMARY KEY (`id_comprobante_pago`),
  KEY `fk_comprobante_pago_suscripcion1_idx` (`id_suscripcion_fk`),
  CONSTRAINT `fk_comprobante_pago_suscripcion1` FOREIGN KEY (`id_suscripcion_fk`) REFERENCES `suscripcion` (`id_suscripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comprobante_pago`
--

LOCK TABLES `comprobante_pago` WRITE;
/*!40000 ALTER TABLE `comprobante_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `comprobante_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credito`
--

DROP TABLE IF EXISTS `credito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `credito` (
  `id_credito` int NOT NULL AUTO_INCREMENT,
  `numero_tarjeta` float NOT NULL,
  `cvv` int NOT NULL,
  `propietario` varchar(100) NOT NULL,
  `tipo_tarjeta` varchar(100) NOT NULL,
  `fecha_caducidad` date NOT NULL,
  `id_opc_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_credito`),
  KEY `fk_opc_debito_opc_pago1_idx` (`id_opc_pago_fk`),
  CONSTRAINT `fk_opc_debito_opc_pago10` FOREIGN KEY (`id_opc_pago_fk`) REFERENCES `opc_pago` (`id_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credito`
--

LOCK TABLES `credito` WRITE;
/*!40000 ALTER TABLE `credito` DISABLE KEYS */;
/*!40000 ALTER TABLE `credito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curso` (
  `id_curso` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `credito` int NOT NULL,
  `horario` date NOT NULL,
  `id_intructor_fk` int NOT NULL,
  PRIMARY KEY (`id_curso`),
  KEY `fk_curso_instructor1_idx` (`id_intructor_fk`),
  CONSTRAINT `fk_curso_instructor1` FOREIGN KEY (`id_intructor_fk`) REFERENCES `instructor` (`id_intructor`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos_alu`
--

DROP TABLE IF EXISTS `cursos_alu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cursos_alu` (
  `id_cursos` int NOT NULL AUTO_INCREMENT,
  `id_alumno_fk` int NOT NULL,
  `id_curso_fk` int NOT NULL,
  PRIMARY KEY (`id_cursos`),
  KEY `fk_cursos_alumno1_idx` (`id_alumno_fk`),
  KEY `fk_cursos_curso1_idx` (`id_curso_fk`),
  CONSTRAINT `fk_cursos_alumno1` FOREIGN KEY (`id_alumno_fk`) REFERENCES `alumno` (`id_alumno`),
  CONSTRAINT `fk_cursos_curso1` FOREIGN KEY (`id_curso_fk`) REFERENCES `curso` (`id_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos_alu`
--

LOCK TABLES `cursos_alu` WRITE;
/*!40000 ALTER TABLE `cursos_alu` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursos_alu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dato_usuario`
--

DROP TABLE IF EXISTS `dato_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dato_usuario` (
  `id_dato_user` int NOT NULL AUTO_INCREMENT,
  `nombre_user` varchar(100) NOT NULL,
  `apellido_pat_user` varchar(100) NOT NULL,
  `apellido_mat_user` varchar(100) NOT NULL,
  `edad_user` int NOT NULL,
  `id_direccion_fk` int NOT NULL,
  `id_plataforma_fk` int NOT NULL,
  PRIMARY KEY (`id_dato_user`),
  KEY `fk_dato_usuario_direccion1_idx` (`id_direccion_fk`),
  KEY `fk_dato_usuario_plataforma1_idx` (`id_plataforma_fk`),
  CONSTRAINT `fk_dato_usuario_direccion1` FOREIGN KEY (`id_direccion_fk`) REFERENCES `direccion` (`id`),
  CONSTRAINT `fk_dato_usuario_plataforma1` FOREIGN KEY (`id_plataforma_fk`) REFERENCES `plataforma` (`id_plataforma`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dato_usuario`
--

LOCK TABLES `dato_usuario` WRITE;
/*!40000 ALTER TABLE `dato_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `dato_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debito`
--

DROP TABLE IF EXISTS `debito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `debito` (
  `id_debito` int NOT NULL AUTO_INCREMENT,
  `numero_tarjeta` float NOT NULL,
  `cvv` int NOT NULL,
  `propietario` varchar(100) NOT NULL,
  `tipo_tarjeta` varchar(100) NOT NULL,
  `fecha_caducidad` date NOT NULL,
  `id_opc_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_debito`),
  KEY `fk_opc_debito_opc_pago1_idx` (`id_opc_pago_fk`),
  CONSTRAINT `fk_opc_debito_opc_pago1` FOREIGN KEY (`id_opc_pago_fk`) REFERENCES `opc_pago` (`id_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debito`
--

LOCK TABLES `debito` WRITE;
/*!40000 ALTER TABLE `debito` DISABLE KEYS */;
/*!40000 ALTER TABLE `debito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direccion`
--

DROP TABLE IF EXISTS `direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `direccion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `calle_d` varchar(100) NOT NULL,
  `numero_d` int NOT NULL,
  `codigo_postal_d` varchar(100) NOT NULL,
  `id_colonia_fk` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_direccion_colonia1_idx` (`id_colonia_fk`),
  CONSTRAINT `fk_direccion_colonia1` FOREIGN KEY (`id_colonia_fk`) REFERENCES `colonia` (`id_colonia`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direccion`
--

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleado` (
  `id_empleado` int NOT NULL AUTO_INCREMENT,
  `num_empledo` varchar(45) NOT NULL,
  `tiempo_en_el_puesto` date NOT NULL,
  `id_dato_user_fk` int NOT NULL,
  PRIMARY KEY (`id_empleado`),
  KEY `fk_empleado_dato_user1_idx` (`id_dato_user_fk`),
  CONSTRAINT `fk_empleado_dato_user1` FOREIGN KEY (`id_dato_user_fk`) REFERENCES `dato_usuario` (`id_dato_user`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructor`
--

DROP TABLE IF EXISTS `instructor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instructor` (
  `id_intructor` int NOT NULL AUTO_INCREMENT,
  `dedicacion` varchar(100) NOT NULL,
  `id_intructor_nuv_fk` int DEFAULT NULL,
  `id_empleado_fk` int NOT NULL,
  PRIMARY KEY (`id_intructor`),
  KEY `fk_instructor_intructor_nuv1_idx` (`id_intructor_nuv_fk`),
  KEY `fk_instructor_empleado1_idx` (`id_empleado_fk`),
  CONSTRAINT `fk_instructor_empleado1` FOREIGN KEY (`id_empleado_fk`) REFERENCES `empleado` (`id_empleado`),
  CONSTRAINT `fk_instructor_intructor_nuv1` FOREIGN KEY (`id_intructor_nuv_fk`) REFERENCES `intructor_nuv` (`id_intructor_nuv`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructor`
--

LOCK TABLES `instructor` WRITE;
/*!40000 ALTER TABLE `instructor` DISABLE KEYS */;
/*!40000 ALTER TABLE `instructor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intructor_nuv`
--

DROP TABLE IF EXISTS `intructor_nuv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `intructor_nuv` (
  `id_intructor_nuv` int NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `experencia` varchar(45) NOT NULL,
  `id_tipo_alumno_fk` int NOT NULL,
  PRIMARY KEY (`id_intructor_nuv`),
  KEY `fk_intructor_nuv_tipo_alumno1_idx` (`id_tipo_alumno_fk`),
  CONSTRAINT `fk_intructor_nuv_tipo_alumno1` FOREIGN KEY (`id_tipo_alumno_fk`) REFERENCES `tipo_alumno` (`id_tipo_alumno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intructor_nuv`
--

LOCK TABLES `intructor_nuv` WRITE;
/*!40000 ALTER TABLE `intructor_nuv` DISABLE KEYS */;
/*!40000 ALTER TABLE `intructor_nuv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipio`
--

DROP TABLE IF EXISTS `municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `municipio` (
  `id_municipio` int NOT NULL AUTO_INCREMENT,
  `nombre_municipio` varchar(45) NOT NULL,
  `id_ciudad_fk` int NOT NULL,
  PRIMARY KEY (`id_municipio`),
  KEY `fk_municipio_ciudad1_idx` (`id_ciudad_fk`),
  CONSTRAINT `fk_municipio_ciudad1` FOREIGN KEY (`id_ciudad_fk`) REFERENCES `ciudad` (`id_ciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipio`
--

LOCK TABLES `municipio` WRITE;
/*!40000 ALTER TABLE `municipio` DISABLE KEYS */;
INSERT INTO `municipio` VALUES (1,'charcho',1),(2,'cuajimalpa',1),(3,'locas',107);
/*!40000 ALTER TABLE `municipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opc_pago`
--

DROP TABLE IF EXISTS `opc_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opc_pago` (
  `id_pago` int NOT NULL AUTO_INCREMENT,
  `monto` varchar(100) NOT NULL,
  `id_suscricion_final_fk` int NOT NULL,
  PRIMARY KEY (`id_pago`),
  KEY `fk_opc_pago_suscricion_final1_idx` (`id_suscricion_final_fk`),
  CONSTRAINT `fk_opc_pago_suscricion_final1` FOREIGN KEY (`id_suscricion_final_fk`) REFERENCES `suscricion_final` (`id_suscricion_final`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opc_pago`
--

LOCK TABLES `opc_pago` WRITE;
/*!40000 ALTER TABLE `opc_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `opc_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pais` (
  `id_pais` int NOT NULL AUTO_INCREMENT,
  `nombre_pais` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'mexico'),(2,'argentina'),(3,'colobia'),(4,'chile'),(5,'japon'),(117,'España'),(118,'Australea'),(119,'Australea');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal`
--

DROP TABLE IF EXISTS `paypal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paypal` (
  `id_opc_paypal` int NOT NULL AUTO_INCREMENT,
  `cuenta` varchar(100) NOT NULL,
  `id_opc_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_opc_paypal`),
  KEY `fk_opc_paypal_opc_pago1_idx` (`id_opc_pago_fk`),
  CONSTRAINT `fk_opc_paypal_opc_pago1` FOREIGN KEY (`id_opc_pago_fk`) REFERENCES `opc_pago` (`id_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal`
--

LOCK TABLES `paypal` WRITE;
/*!40000 ALTER TABLE `paypal` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plataforma`
--

DROP TABLE IF EXISTS `plataforma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plataforma` (
  `id_plataforma` int NOT NULL AUTO_INCREMENT,
  `nombre_plataforma` varchar(100) NOT NULL,
  PRIMARY KEY (`id_plataforma`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plataforma`
--

LOCK TABLES `plataforma` WRITE;
/*!40000 ALTER TABLE `plataforma` DISABLE KEYS */;
INSERT INTO `plataforma` VALUES (1,'RedYou'),(2,'Fifi'),(6,'Los del barrio');
/*!40000 ALTER TABLE `plataforma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suscricion_final`
--

DROP TABLE IF EXISTS `suscricion_final`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `suscricion_final` (
  `id_suscricion_final` int NOT NULL,
  `prueba` date DEFAULT NULL,
  `trimestre` date DEFAULT NULL,
  `semestre` date DEFAULT NULL,
  `anual` date DEFAULT NULL,
  `id_suscripcion_fk` int NOT NULL,
  PRIMARY KEY (`id_suscricion_final`),
  KEY `fk_suscricion_final_suscripcion1_idx` (`id_suscripcion_fk`),
  CONSTRAINT `fk_suscricion_final_suscripcion1` FOREIGN KEY (`id_suscripcion_fk`) REFERENCES `suscripcion` (`id_suscripcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suscricion_final`
--

LOCK TABLES `suscricion_final` WRITE;
/*!40000 ALTER TABLE `suscricion_final` DISABLE KEYS */;
/*!40000 ALTER TABLE `suscricion_final` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suscripcion`
--

DROP TABLE IF EXISTS `suscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `suscripcion` (
  `id_suscripcion` int NOT NULL AUTO_INCREMENT,
  `inicio` date NOT NULL,
  `id_alumno_fk` int NOT NULL,
  PRIMARY KEY (`id_suscripcion`),
  KEY `fk_suscripcion_alumno1_idx` (`id_alumno_fk`),
  CONSTRAINT `fk_suscripcion_alumno1` FOREIGN KEY (`id_alumno_fk`) REFERENCES `alumno` (`id_alumno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suscripcion`
--

LOCK TABLES `suscripcion` WRITE;
/*!40000 ALTER TABLE `suscripcion` DISABLE KEYS */;
/*!40000 ALTER TABLE `suscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_alumno`
--

DROP TABLE IF EXISTS `tipo_alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_alumno` (
  `id_tipo_alumno` int NOT NULL AUTO_INCREMENT,
  `años_de_inicio` varchar(100) NOT NULL,
  `matricula_alu` varchar(100) NOT NULL,
  `aprendisage` varchar(100) NOT NULL,
  `horas_cumpliudas` date NOT NULL,
  `id_dato_user_fk` int NOT NULL,
  PRIMARY KEY (`id_tipo_alumno`),
  KEY `fk_tipo_alumno_dato_usuario1_idx` (`id_dato_user_fk`),
  CONSTRAINT `fk_tipo_alumno_dato_usuario1` FOREIGN KEY (`id_dato_user_fk`) REFERENCES `dato_usuario` (`id_dato_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_alumno`
--

LOCK TABLES `tipo_alumno` WRITE;
/*!40000 ALTER TABLE `tipo_alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vinculacion`
--

DROP TABLE IF EXISTS `vinculacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vinculacion` (
  `id_vinculacion` int NOT NULL AUTO_INCREMENT,
  `nom_vinculo` varchar(100) NOT NULL,
  `contraseña` varchar(100) NOT NULL,
  `facebook` varchar(45) DEFAULT NULL,
  `github` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `id_dato_user_fk` int NOT NULL,
  PRIMARY KEY (`id_vinculacion`),
  KEY `fk_vinculacion_dato_usuario1_idx` (`id_dato_user_fk`),
  CONSTRAINT `fk_vinculacion_dato_usuario1` FOREIGN KEY (`id_dato_user_fk`) REFERENCES `dato_usuario` (`id_dato_user`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vinculacion`
--

LOCK TABLES `vinculacion` WRITE;
/*!40000 ALTER TABLE `vinculacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `vinculacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'curso_online'
--

--
-- Dumping routines for database 'curso_online'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-29 16:54:27
