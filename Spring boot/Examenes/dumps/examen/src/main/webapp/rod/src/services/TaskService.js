import axios from "axios";

const USER_REST_API_URL = "http://localhost:8080/todolist/tasks"

class TaskService{


    getAllTasks(){
        return axios.get(USER_REST_API_URL);
    }

    getTasksById(id){
        return axios.get(USER_REST_API_URL + '/' + id);
    }

    createTasks(task){
        return axios.post(USER_REST_API_URL,task);
    }

    updateTasks(id,task){
        return axios.post(USER_REST_API_URL + '/' + id, task);
    }

    deleteAllTasks(){
        return axios.delete(USER_REST_API_URL);
    }

    deleteTasksById(id){
        return axios.delete(USER_REST_API_URL + '/' + id);
    }

}

export default new TaskService()