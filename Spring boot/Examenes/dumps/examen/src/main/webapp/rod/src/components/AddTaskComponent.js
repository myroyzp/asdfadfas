import React, {useState, useEffect} from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import TaskService from '../services/TaskService'

const AddTaskComponent = () => {

    const [calle, setCalle] = useState('')
    const [numero, setNumero] = useState('')
    const [codigo_postal, setCP] = useState('')
    const navigate = useNavigate();
    const {id} = useParams();

    const saveorUpdateTask = (e) =>{
        e.preventDefault();
        const task = {calle,numero,codigo_postal}
        if(id){
            TaskService.updateTasks(id,task).then((response)=> {
                navigate('/tasks',{replace: true})
            }).catch(error => {
                console.log(error)
            })
        }else{
            TaskService.createTasks(task).then((response) =>{
                console.log(response.data)
                navigate('/tasks',{replace: true})
            }).catch(error => {
                console.log(error)
            })
        }
       

    }
    useEffect(() => {
       TaskService.getTasksById(id).then((response) => {       
        setCalle(response.data.calle)
        setNumero(response.data.numero)
        setCP(response.data.codigo_postal)

       }).catch(error =>{
           console.log(error)
       })
    }, [])

    const title = () =>{
        if(id){
            return <h2 className='text-center'>Update</h2>
        }else{
            <h2 className='text-center'>Add Task</h2>
        }
    }

    return (
        <div>
            <br/><br/>
            <div className='container'>
                <div className='row'>
                    <div className='card col-md-2 offset-md-5 offset-md-4'>
                    {
                        title
                    }
                        <div className='card-body'>
                            <form>
                                <div className='from-group mb-2'>
                                    <label className='form-label'>Calle</label>
                                    <input className='from-control' type='text' placeholder='Enter Calle' name='calle'
                                    value={calle} onChange={(e) => setCalle(e.target.value)}></input>
                                    <label className='form-label'>Numero</label>
                                    <input className='from-control' type='text' placeholder='Enter Numero' name='numero'
                                    value={numero} onChange={(e) => setNumero(e.target.value)}></input>
                                    <label className='form-label'>Codigo Postal</label>
                                    <input className='from-control' type='text' placeholder='Enter Codigo postal' name='codigo_postal'
                                    value={codigo_postal} onChange={(e) => setCP(e.target.value)}></input>
                                </div>
                                <button className='btn btn-success' onClick={(e) => saveorUpdateTask(e)}>Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AddTaskComponent;