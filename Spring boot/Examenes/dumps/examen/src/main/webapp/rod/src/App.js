import './App.css';
import { BrowserRouter as Router,Route,Routes} from 'react-router-dom';
import ListTaskComponent from './components/ListTaskComponent'
import HeaderComponet from './components/HeaderComponet'
import FooterComponetnt from './components/FooterComponetnt'
import AddTaskComponent from './components/AddTaskComponent'

function App() {
  return (
    <div>
      <Router>
        <HeaderComponet/>
        <div className='container'>
          <Routes>
            <Route path='/' element={<ListTaskComponent/>}></Route>
            <Route path='/tasks' element={<ListTaskComponent/>}></Route>
            <Route path='/add-task' element={<AddTaskComponent/>}></Route>
            <Route path='/edit-task/:id' element={<AddTaskComponent/>}></Route>
          </Routes>
        </div>
        <FooterComponetnt/>
      </Router>
    </div>
  );
}

export default App;
