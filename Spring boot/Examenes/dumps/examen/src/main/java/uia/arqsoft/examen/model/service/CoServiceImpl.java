package uia.arqsoft.examen.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.arqsoft.examen.model.dao.CoDao;
import uia.arqsoft.examen.model.entity.Cursos_only;

import java.util.List;
import java.util.Optional;

@Service
public class CoServiceImpl implements CoService {

    @Autowired
    private CoDao CoDao;

    @Override
    public List<Cursos_only> getCursos() {
        return (List<Cursos_only>) CoDao.findAll();
    }

    @Override
    public Optional<Cursos_only> getCursosByid(Long id) {
        return CoDao.findById(id);
    }

    @Override
    public void deleteById(Long id){
        CoDao.deleteById(id);
    }


    @Override
    public void deleteAll(){
        CoDao.deleteAll();
    }

    @Override
    public Cursos_only save(Cursos_only Cur_only) {
        CoDao.save(Cur_only);
        return Cur_only;
    }
}
