package uia.arqsoft.examen.model.contoller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.examen.model.entity.Cursos_only;
import uia.arqsoft.examen.model.service.CoService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
@RequestMapping(value = "list")
public class PsList {
    @Autowired
    private CoService taskService;

    @GetMapping(value = "tasks")
    public String getTasks(Model model){

        List<Cursos_only> list = taskService.getCursos();
        model.addAttribute("table",list);
       return "tasks/index";
    }
    @GetMapping(value = "tasks/{id}")
    public String getTaskById(@PathVariable(name = "id") Long id, Model model ){
        Optional<Cursos_only> task = taskService.getCursosByid(id);
        if (task.isPresent())
        {
            List<Cursos_only> list = new ArrayList<>();
            list.add(task.get());
            log.info("Controller - getCursosById - task{}",list);
            model.addAttribute("table",list);
        }
        else{
            model.addAttribute("table",null);
        }
        return "tasks/index";
    }
    @GetMapping(value = "delete/{id}")
    public String deleteTaskById(@PathVariable(name = "id") Long id ,Model model){
        taskService.deleteById(id);
        List<Cursos_only> list = taskService.getCursos();
        model.addAttribute("table",list);
        return "tasks/index";
    }

    @GetMapping(value = "new")
    public String newTask(Model model){
        Cursos_only myTask = new Cursos_only();
        model.addAttribute("TabelDomicilio",myTask);
        return "tasks/form";
    }


    @GetMapping(value = "update/{id}")
    public String updateTask(@PathVariable(name = "id") Long id ,Model model)throws IOException {
        Optional<Cursos_only> taskData = taskService.getCursosByid(id);

        if(taskData.isPresent()){
            Cursos_only myTask = taskData.get();
            model.addAttribute("TabelDomicilio",myTask);

        }else {
            model.addAttribute("table",null);
        }

        return "tasks/form";
    }


    @PostMapping(value = "save")
    public String saveTask(@ModelAttribute("TabelDomicilio") Cursos_only task, Model model)throws IOException {
        log.info("Controller - save - task {}", task);

        Cursos_only myTask = taskService.save(task);
        List<Cursos_only> list = new ArrayList<>();
        list.add(myTask);
        model.addAttribute("table",list);
        /*taskService.save(task);*/
        return "tasks/index";
    }
}
