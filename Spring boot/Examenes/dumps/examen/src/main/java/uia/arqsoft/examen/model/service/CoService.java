package uia.arqsoft.examen.model.service;

import uia.arqsoft.examen.model.entity.Cursos_only;
import java.util.List;
import java.util.Optional;

public interface CoService {
    List<Cursos_only> getCursos();
    Optional<Cursos_only> getCursosByid(Long id);
    void deleteById(Long id);
    void deleteAll();
    Cursos_only save(Cursos_only task);

}
