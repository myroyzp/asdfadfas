package uia.arqsoft.examen.model.exception;

public class CoNotFoundException extends Exception{
    public CoNotFoundException(String message){
        super(message);

    }
}
