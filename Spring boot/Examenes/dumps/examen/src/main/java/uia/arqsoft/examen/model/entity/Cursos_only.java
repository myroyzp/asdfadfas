package uia.arqsoft.examen.model.entity;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "direccion")
public class Cursos_only implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id/*el id se usa para tener una PK*/
    /*GeneratedValue se usa para auto incrementar*/
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String calle;
    private Integer numero;
    private String codigo_postal;

    public Cursos_only(String calle, Integer numero, String codigo_postal) {
        this.id = id;
        this.calle = calle;
        this.numero = numero;
        this.codigo_postal = codigo_postal;
    }

    public Cursos_only() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getCodigo_postal() {
        return codigo_postal;
    }

    public void setCodigo_postal(String codigo_postal) {
        this.codigo_postal = codigo_postal;
    }



}

