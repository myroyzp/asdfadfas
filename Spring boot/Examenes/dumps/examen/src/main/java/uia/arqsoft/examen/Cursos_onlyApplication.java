package uia.arqsoft.examen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cursos_onlyApplication {

    public static void main(String[] args) {
        SpringApplication.run(Cursos_onlyApplication.class, args);
    }

}
