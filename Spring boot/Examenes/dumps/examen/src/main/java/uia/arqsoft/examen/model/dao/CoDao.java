package uia.arqsoft.examen.model.dao;

import org.springframework.data.repository.CrudRepository;
import uia.arqsoft.examen.model.entity.Cursos_only;

public interface CoDao extends CrudRepository<Cursos_only, Long> {

}

