package uia.arqsoft.examen.model.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uia.arqsoft.examen.model.entity.Cursos_only;
import uia.arqsoft.examen.model.service.CoService;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping(value = "todolist")
public class PsListRest {
    @Autowired
    private CoService CorService;

    @GetMapping(value = "tasks")
    public List<Cursos_only> getTasks(){
        return CorService.getCursos();
    }

    @GetMapping(value = "tasks/{id}")
    public Optional<Cursos_only> getTaskById(@PathVariable(name = "id") Long id){
        return CorService.getCursosByid(id);
    }

    @DeleteMapping(value = "tasks/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable(name = "id")Long id){
        try{
            CorService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping(value = "tasks")
    public ResponseEntity<HttpStatus> deleteAll(){
        try{
            CorService.deleteAll();
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/tasks")
    public ResponseEntity<Cursos_only> create (@RequestBody Cursos_only task){
        try {
            Cursos_only myTask = CorService.save(new Cursos_only(task.getCalle(),task.getNumero(),task.getCodigo_postal()));
            return new ResponseEntity<>(myTask, HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/tasks/{id}")
    public ResponseEntity<Cursos_only> modify (@PathVariable(name = "id") Long id, @RequestBody Cursos_only task){
        Optional<Cursos_only> taskData = CorService.getCursosByid(id);

        if(taskData.isPresent()){
            Cursos_only myTask = taskData.get();
           myTask.setCalle(task.getCalle());
           myTask.setNumero(task.getNumero());
           myTask.setCodigo_postal(task.getCodigo_postal());

            return new ResponseEntity<>(CorService.save(myTask), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}
