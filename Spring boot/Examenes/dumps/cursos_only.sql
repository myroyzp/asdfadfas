-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: curso_linea
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id_admin` int NOT NULL AUTO_INCREMENT,
  `empleado_id_empleado` int NOT NULL,
  `dato_user_id_dato_user` int NOT NULL,
  `plataforma_id_plataforma` int NOT NULL,
  PRIMARY KEY (`id_admin`),
  KEY `fk_admin_empleado1_idx` (`empleado_id_empleado`),
  KEY `fk_admin_dato_user1_idx` (`dato_user_id_dato_user`),
  KEY `fk_admin_plataforma1_idx` (`plataforma_id_plataforma`),
  CONSTRAINT `fk_admin_dato_user1` FOREIGN KEY (`dato_user_id_dato_user`) REFERENCES `dato_user` (`id_dato_user`),
  CONSTRAINT `fk_admin_empleado1` FOREIGN KEY (`empleado_id_empleado`) REFERENCES `empleado` (`id_empleado`),
  CONSTRAINT `fk_admin_plataforma1` FOREIGN KEY (`plataforma_id_plataforma`) REFERENCES `plataforma` (`id_plataforma`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,2,11,5),(2,3,12,5),(3,4,13,3),(4,6,14,4),(5,7,15,1);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumno` (
  `id_alumno` int NOT NULL AUTO_INCREMENT,
  `id_dato_user_fk` int NOT NULL,
  PRIMARY KEY (`id_alumno`),
  KEY `fk_alumno_dato_user1_idx` (`id_dato_user_fk`),
  CONSTRAINT `fk_alumno_dato_user1` FOREIGN KEY (`id_dato_user_fk`) REFERENCES `dato_user` (`id_dato_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES (1,1),(2,2),(3,3),(4,4),(5,5);
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anual`
--

DROP TABLE IF EXISTS `anual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `anual` (
  `id_anual` int NOT NULL AUTO_INCREMENT,
  `anual` date NOT NULL,
  `suscripcion_id_suscripcion` int NOT NULL,
  PRIMARY KEY (`id_anual`),
  KEY `fk_anual_suscripcion1_idx` (`suscripcion_id_suscripcion`),
  CONSTRAINT `fk_anual_suscripcion1` FOREIGN KEY (`suscripcion_id_suscripcion`) REFERENCES `suscripcion` (`id_suscripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anual`
--

LOCK TABLES `anual` WRITE;
/*!40000 ALTER TABLE `anual` DISABLE KEYS */;
INSERT INTO `anual` VALUES (1,'2022-11-16',2);
/*!40000 ALTER TABLE `anual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudad` (
  `id_ciudad` int NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(100) NOT NULL,
  `id_colonia_fk` int NOT NULL,
  PRIMARY KEY (`id_ciudad`),
  KEY `fk_ciudad_colonia1_idx` (`id_colonia_fk`),
  CONSTRAINT `fk_ciudad_colonia1` FOREIGN KEY (`id_colonia_fk`) REFERENCES `colonia` (`id_colonia`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'cdmx',1),(2,'rio de janero',2),(3,'tokyo',3),(4,'londres',4),(5,'buenos aires',5);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clase`
--

DROP TABLE IF EXISTS `clase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clase` (
  `id_clase` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `credito` int NOT NULL,
  PRIMARY KEY (`id_clase`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clase`
--

LOCK TABLES `clase` WRITE;
/*!40000 ALTER TABLE `clase` DISABLE KEYS */;
INSERT INTO `clase` VALUES (1,'informatica',12),(2,'matematicas',22),(3,'java de 0 a 100',10),(4,'mysql',5),(5,'programcion',4),(6,'base',5);
/*!40000 ALTER TABLE `clase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claseal`
--

DROP TABLE IF EXISTS `claseal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `claseal` (
  `alumno_id_alumno` int NOT NULL,
  `clase_id_clase` int NOT NULL,
  PRIMARY KEY (`alumno_id_alumno`,`clase_id_clase`),
  KEY `fk_claseAl_alumno1_idx` (`alumno_id_alumno`),
  KEY `fk_claseAl_clase1_idx` (`clase_id_clase`),
  CONSTRAINT `fk_claseAl_alumno1` FOREIGN KEY (`alumno_id_alumno`) REFERENCES `alumno` (`id_alumno`),
  CONSTRAINT `fk_claseAl_clase1` FOREIGN KEY (`clase_id_clase`) REFERENCES `clase` (`id_clase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claseal`
--

LOCK TABLES `claseal` WRITE;
/*!40000 ALTER TABLE `claseal` DISABLE KEYS */;
INSERT INTO `claseal` VALUES (1,5),(2,3),(3,5),(4,3),(5,2);
/*!40000 ALTER TABLE `claseal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clasepr`
--

DROP TABLE IF EXISTS `clasepr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clasepr` (
  `id_clase` int DEFAULT NULL,
  `nombre` text,
  `credito` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clasepr`
--

LOCK TABLES `clasepr` WRITE;
/*!40000 ALTER TABLE `clasepr` DISABLE KEYS */;
INSERT INTO `clasepr` VALUES (1,'informatica',12),(2,'matematicas',22),(3,'java de 0 a 100',10),(4,'mysql',5),(5,'programcion',4);
/*!40000 ALTER TABLE `clasepr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colonia`
--

DROP TABLE IF EXISTS `colonia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `colonia` (
  `id_colonia` int NOT NULL AUTO_INCREMENT,
  `colonia` varchar(100) NOT NULL,
  `id_direccion_fk` int NOT NULL,
  PRIMARY KEY (`id_colonia`),
  KEY `fk_colonia_direccion_idx` (`id_direccion_fk`),
  CONSTRAINT `fk_colonia_direccion` FOREIGN KEY (`id_direccion_fk`) REFERENCES `direccion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colonia`
--

LOCK TABLES `colonia` WRITE;
/*!40000 ALTER TABLE `colonia` DISABLE KEYS */;
INSERT INTO `colonia` VALUES (1,'mecla',1),(2,'veracrus',2),(3,'oyasumi',3),(4,'polom',4),(5,'bueno aires',5);
/*!40000 ALTER TABLE `colonia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comprobante_pago`
--

DROP TABLE IF EXISTS `comprobante_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comprobante_pago` (
  `id_comprobante_pago` int NOT NULL AUTO_INCREMENT,
  `comprobante_pagocol` varchar(45) NOT NULL,
  `id_correo_fk` int NOT NULL,
  `id_opc_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_comprobante_pago`),
  KEY `fk_comprobante_pago_correo1_idx` (`id_correo_fk`),
  KEY `fk_comprobante_pago_opc_pago1_idx` (`id_opc_pago_fk`),
  CONSTRAINT `fk_comprobante_pago_correo1` FOREIGN KEY (`id_correo_fk`) REFERENCES `correo` (`id_correo`),
  CONSTRAINT `fk_comprobante_pago_opc_pago1` FOREIGN KEY (`id_opc_pago_fk`) REFERENCES `opc_pago` (`id_opc_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comprobante_pago`
--

LOCK TABLES `comprobante_pago` WRITE;
/*!40000 ALTER TABLE `comprobante_pago` DISABLE KEYS */;
INSERT INTO `comprobante_pago` VALUES (1,'gracias por la compra: semestra',1,1),(2,'gracias por la compra: anual',2,3),(3,'gracias por la compra: prueba gratis',5,2),(4,'gracias por la compra: trimestra',3,4),(5,'gracias por la compra: semestra',4,5);
/*!40000 ALTER TABLE `comprobante_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correo`
--

DROP TABLE IF EXISTS `correo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `correo` (
  `id_correo` int NOT NULL AUTO_INCREMENT,
  `correo` varchar(100) NOT NULL,
  `id_vinculacion_fk` int NOT NULL,
  PRIMARY KEY (`id_correo`),
  KEY `fk_correo_vinculacion1_idx` (`id_vinculacion_fk`),
  CONSTRAINT `fk_correo_vinculacion1` FOREIGN KEY (`id_vinculacion_fk`) REFERENCES `vinculacion` (`id_vinculacion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correo`
--

LOCK TABLES `correo` WRITE;
/*!40000 ALTER TABLE `correo` DISABLE KEYS */;
INSERT INTO `correo` VALUES (1,'njaskowicz0@w3.org',6),(2,'gheritege1@storify.com',7),(3,'raldine2@arstechnica.com',8),(4,'nmcowen3@rediff.com	',9),(5,'tmenco4@ox.ac.uk	',10);
/*!40000 ALTER TABLE `correo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dato_user`
--

DROP TABLE IF EXISTS `dato_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dato_user` (
  `id_dato_user` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido_pat` varchar(100) NOT NULL,
  `apellido_mat` varchar(100) NOT NULL,
  `edad` int NOT NULL,
  `id_ubicacion_fk` int NOT NULL,
  `id_inicio_sesion_fk` int NOT NULL,
  `id_plataforma_fk` int NOT NULL,
  PRIMARY KEY (`id_dato_user`),
  KEY `fk_dato_user_ubicacion1_idx` (`id_ubicacion_fk`),
  KEY `fk_dato_user_inicio_sesion1_idx` (`id_inicio_sesion_fk`),
  KEY `fk_dato_user_plataforma1_idx` (`id_plataforma_fk`),
  CONSTRAINT `fk_dato_user_inicio_sesion1` FOREIGN KEY (`id_inicio_sesion_fk`) REFERENCES `inicio_sesion` (`id_inicio_sesion`),
  CONSTRAINT `fk_dato_user_plataforma1` FOREIGN KEY (`id_plataforma_fk`) REFERENCES `plataforma` (`id_plataforma`),
  CONSTRAINT `fk_dato_user_ubicacion1` FOREIGN KEY (`id_ubicacion_fk`) REFERENCES `ubicacion` (`id_ubicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dato_user`
--

LOCK TABLES `dato_user` WRITE;
/*!40000 ALTER TABLE `dato_user` DISABLE KEYS */;
INSERT INTO `dato_user` VALUES (1,'Sansone','Hanhart','MacFarlan',34,1,10,1),(2,'Bastien','Skey','Macguire',45,2,8,5),(3,'Gonzalo','Hinksen','Huguet',20,2,9,3),(4,'Marquita','Jelleman','Bucknall',78,5,7,2),(5,'Wilhelmina','Litton','Mengue',18,1,13,5),(6,'Delmer','Bitcheno','Bingell',15,5,6,4),(7,'Orren','Ninnoli','Speck',40,3,5,2),(8,'Glynnis','Syversen','Faithfull',65,4,4,3),(9,'Kermy','Zanetto','Lobley',45,2,2,5),(10,'Gabie','Gerriets','Ionnidis',34,1,1,4),(11,'Sanford','Phonix','Smythin',16,3,3,2),(12,'Drucy','Claiton','Stonnell',45,4,15,1),(13,'Sarette','Colbert','Luten',12,5,14,3),(14,'Benton','Boyford','Wastall',65,2,12,5),(15,'Reinaldos','Boyford','Rieflin',40,3,11,4);
/*!40000 ALTER TABLE `dato_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direccion`
--

DROP TABLE IF EXISTS `direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `direccion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `calle` varchar(100) NOT NULL,
  `numero` int NOT NULL,
  `codigo_postal` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direccion`
--

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
INSERT INTO `direccion` VALUES (1,'tepito',666,'66657'),(2,'macako',646,'44592'),(3,'atita',4568,'1125'),(4,'polm',11584,'1245644'),(5,'buenos aires',44549,'1124');
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleado` (
  `id_empleado` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`id_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'maestro'),(2,'adminstrdor'),(3,'adminstrdor'),(4,'adminstrdor'),(5,'meastro'),(6,'adminstrdor'),(7,'adminstrdor'),(8,'maestro'),(9,'maestro'),(10,'maestro');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facebook`
--

DROP TABLE IF EXISTS `facebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `facebook` (
  `id_facebook` int NOT NULL AUTO_INCREMENT,
  `facebook` varchar(100) NOT NULL,
  `id_vinculacion_fk` int NOT NULL,
  PRIMARY KEY (`id_facebook`),
  KEY `fk_facebook_vinculacion1_idx` (`id_vinculacion_fk`),
  CONSTRAINT `fk_facebook_vinculacion1` FOREIGN KEY (`id_vinculacion_fk`) REFERENCES `vinculacion` (`id_vinculacion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facebook`
--

LOCK TABLES `facebook` WRITE;
/*!40000 ALTER TABLE `facebook` DISABLE KEYS */;
INSERT INTO `facebook` VALUES (1,'Male to Female	',1),(2,'Intersex',2),(3,'Agender',3),(4,'Variant',4),(5,'Trans ',5);
/*!40000 ALTER TABLE `facebook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `github`
--

DROP TABLE IF EXISTS `github`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `github` (
  `id_github` int NOT NULL AUTO_INCREMENT,
  `github` varchar(100) NOT NULL,
  `vinculacion_id_vinculacion` int NOT NULL,
  PRIMARY KEY (`id_github`),
  KEY `fk_github_vinculacion1_idx` (`vinculacion_id_vinculacion`),
  CONSTRAINT `fk_github_vinculacion1` FOREIGN KEY (`vinculacion_id_vinculacion`) REFERENCES `vinculacion` (`id_vinculacion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `github`
--

LOCK TABLES `github` WRITE;
/*!40000 ALTER TABLE `github` DISABLE KEYS */;
INSERT INTO `github` VALUES (1,'Cisgender',11),(2,'Cis Male',12),(3,'Transgender Male',13),(4,'Transsexual',14),(5,'Transmasculine',15);
/*!40000 ALTER TABLE `github` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inicio_sesion`
--

DROP TABLE IF EXISTS `inicio_sesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inicio_sesion` (
  `id_inicio_sesion` int NOT NULL AUTO_INCREMENT,
  `id_vinculacion_fk` int NOT NULL,
  PRIMARY KEY (`id_inicio_sesion`),
  KEY `fk_inicio_sesion_vinculacion1_idx` (`id_vinculacion_fk`),
  CONSTRAINT `fk_inicio_sesion_vinculacion1` FOREIGN KEY (`id_vinculacion_fk`) REFERENCES `vinculacion` (`id_vinculacion`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inicio_sesion`
--

LOCK TABLES `inicio_sesion` WRITE;
/*!40000 ALTER TABLE `inicio_sesion` DISABLE KEYS */;
INSERT INTO `inicio_sesion` VALUES (15,1),(14,2),(13,3),(12,4),(11,5),(10,6),(9,7),(8,8),(7,9),(6,10),(5,11),(4,12),(3,13),(2,14),(1,15);
/*!40000 ALTER TABLE `inicio_sesion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opc_credito`
--

DROP TABLE IF EXISTS `opc_credito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opc_credito` (
  `id_cardcredito` int NOT NULL AUTO_INCREMENT,
  `cardcredito` float NOT NULL,
  `id_opc_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_cardcredito`),
  KEY `fk_opc_cardcredito_opc_pago1_idx` (`id_opc_pago_fk`),
  CONSTRAINT `fk_opc_cardcredito_opc_pago1` FOREIGN KEY (`id_opc_pago_fk`) REFERENCES `opc_pago` (`id_opc_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opc_credito`
--

LOCK TABLES `opc_credito` WRITE;
/*!40000 ALTER TABLE `opc_credito` DISABLE KEYS */;
INSERT INTO `opc_credito` VALUES (1,99.99,5);
/*!40000 ALTER TABLE `opc_credito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opc_debito`
--

DROP TABLE IF EXISTS `opc_debito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opc_debito` (
  `id_opc_debito` int NOT NULL AUTO_INCREMENT,
  `card_debito` float NOT NULL,
  `id_opc_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_opc_debito`),
  KEY `fk_opc_debito_opc_pago1_idx` (`id_opc_pago_fk`),
  CONSTRAINT `fk_opc_debito_opc_pago1` FOREIGN KEY (`id_opc_pago_fk`) REFERENCES `opc_pago` (`id_opc_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opc_debito`
--

LOCK TABLES `opc_debito` WRITE;
/*!40000 ALTER TABLE `opc_debito` DISABLE KEYS */;
INSERT INTO `opc_debito` VALUES (1,15.99,3);
/*!40000 ALTER TABLE `opc_debito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opc_pago`
--

DROP TABLE IF EXISTS `opc_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opc_pago` (
  `id_opc_pago` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `id_alumno_fk` int NOT NULL,
  PRIMARY KEY (`id_opc_pago`),
  KEY `fk_opc_pago_alumno1_idx` (`id_alumno_fk`),
  CONSTRAINT `fk_opc_pago_alumno1` FOREIGN KEY (`id_alumno_fk`) REFERENCES `alumno` (`id_alumno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opc_pago`
--

LOCK TABLES `opc_pago` WRITE;
/*!40000 ALTER TABLE `opc_pago` DISABLE KEYS */;
INSERT INTO `opc_pago` VALUES (1,'te llegar un tiket ',1),(2,'te llegar un tiket',2),(3,'te llegar un tiket',3),(4,'te llegar un tiket',4),(5,'te llegar un tiket',5);
/*!40000 ALTER TABLE `opc_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opc_paypal`
--

DROP TABLE IF EXISTS `opc_paypal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opc_paypal` (
  `id_opc_paypal` int NOT NULL AUTO_INCREMENT,
  `paypalcol` float NOT NULL,
  `id_opc_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_opc_paypal`),
  KEY `fk_opc_paypal_opc_pago1_idx` (`id_opc_pago_fk`),
  CONSTRAINT `fk_opc_paypal_opc_pago1` FOREIGN KEY (`id_opc_pago_fk`) REFERENCES `opc_pago` (`id_opc_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opc_paypal`
--

LOCK TABLES `opc_paypal` WRITE;
/*!40000 ALTER TABLE `opc_paypal` DISABLE KEYS */;
INSERT INTO `opc_paypal` VALUES (1,110.99,1),(2,50.99,2);
/*!40000 ALTER TABLE `opc_paypal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plataforma`
--

DROP TABLE IF EXISTS `plataforma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plataforma` (
  `id_plataforma` int NOT NULL AUTO_INCREMENT,
  `nom_plataforma` varchar(100) NOT NULL,
  PRIMARY KEY (`id_plataforma`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plataforma`
--

LOCK TABLES `plataforma` WRITE;
/*!40000 ALTER TABLE `plataforma` DISABLE KEYS */;
INSERT INTO `plataforma` VALUES (1,'pin pan pum'),(2,'flhas'),(3,'correcto'),(4,'shhhh'),(5,'wwwwa');
/*!40000 ALTER TABLE `plataforma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profesor` (
  `id_profesor` int NOT NULL AUTO_INCREMENT,
  `timpo_puesto` varchar(100) NOT NULL,
  `id_dato_user_fk` int NOT NULL,
  `id_empleado_fk` int NOT NULL,
  PRIMARY KEY (`id_profesor`),
  KEY `fk_profesor_dato_user1_idx` (`id_dato_user_fk`),
  KEY `fk_profesor_empleado1_idx` (`id_empleado_fk`),
  CONSTRAINT `fk_profesor_dato_user1` FOREIGN KEY (`id_dato_user_fk`) REFERENCES `dato_user` (`id_dato_user`),
  CONSTRAINT `fk_profesor_empleado1` FOREIGN KEY (`id_empleado_fk`) REFERENCES `empleado` (`id_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor`
--

LOCK TABLES `profesor` WRITE;
/*!40000 ALTER TABLE `profesor` DISABLE KEYS */;
INSERT INTO `profesor` VALUES (6,'todo el dia ',10,1),(7,'medio timepo',9,5),(8,'medio timepo',8,8),(9,'todo el dia',7,9),(10,'medio timepo',6,10);
/*!40000 ALTER TABLE `profesor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prueba`
--

DROP TABLE IF EXISTS `prueba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prueba` (
  `id_prueba` int NOT NULL AUTO_INCREMENT,
  `prueba` date NOT NULL,
  `suscripcion_id_suscripcion` int NOT NULL,
  PRIMARY KEY (`id_prueba`),
  KEY `fk_prueba_suscripcion1_idx` (`suscripcion_id_suscripcion`),
  CONSTRAINT `fk_prueba_suscripcion1` FOREIGN KEY (`suscripcion_id_suscripcion`) REFERENCES `suscripcion` (`id_suscripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prueba`
--

LOCK TABLES `prueba` WRITE;
/*!40000 ALTER TABLE `prueba` DISABLE KEYS */;
INSERT INTO `prueba` VALUES (1,'2021-11-23',3);
/*!40000 ALTER TABLE `prueba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semestre`
--

DROP TABLE IF EXISTS `semestre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `semestre` (
  `id_semestre` int NOT NULL AUTO_INCREMENT,
  `semestre` date NOT NULL,
  `suscripcion_id_suscripcion` int NOT NULL,
  PRIMARY KEY (`id_semestre`),
  KEY `fk_semestre_suscripcion1_idx` (`suscripcion_id_suscripcion`),
  CONSTRAINT `fk_semestre_suscripcion1` FOREIGN KEY (`suscripcion_id_suscripcion`) REFERENCES `suscripcion` (`id_suscripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semestre`
--

LOCK TABLES `semestre` WRITE;
/*!40000 ALTER TABLE `semestre` DISABLE KEYS */;
INSERT INTO `semestre` VALUES (1,'2021-05-16',4),(2,'2021-05-16',5);
/*!40000 ALTER TABLE `semestre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suscripcion`
--

DROP TABLE IF EXISTS `suscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `suscripcion` (
  `id_suscripcion` int NOT NULL AUTO_INCREMENT,
  `inicio` date NOT NULL,
  `alumno_id_alumno` int NOT NULL,
  PRIMARY KEY (`id_suscripcion`,`alumno_id_alumno`),
  KEY `fk_suscripcion_alumno1_idx` (`alumno_id_alumno`),
  CONSTRAINT `fk_suscripcion_alumno1` FOREIGN KEY (`alumno_id_alumno`) REFERENCES `alumno` (`id_alumno`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suscripcion`
--

LOCK TABLES `suscripcion` WRITE;
/*!40000 ALTER TABLE `suscripcion` DISABLE KEYS */;
INSERT INTO `suscripcion` VALUES (1,'2021-11-16',1),(2,'2021-11-16',2),(3,'2021-11-16',3),(4,'2021-11-16',4),(5,'2021-11-16',5);
/*!40000 ALTER TABLE `suscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tesoreria`
--

DROP TABLE IF EXISTS `tesoreria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tesoreria` (
  `id_sumatoria` int NOT NULL AUTO_INCREMENT,
  `suma_mes` float NOT NULL,
  `suma_tri` float NOT NULL,
  `suma_anual` float NOT NULL,
  PRIMARY KEY (`id_sumatoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tesoreria`
--

LOCK TABLES `tesoreria` WRITE;
/*!40000 ALTER TABLE `tesoreria` DISABLE KEYS */;
/*!40000 ALTER TABLE `tesoreria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trimestre`
--

DROP TABLE IF EXISTS `trimestre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trimestre` (
  `id_trimestre` int NOT NULL AUTO_INCREMENT,
  `trimestre` date NOT NULL,
  `suscripcion_id_suscripcion` int NOT NULL,
  PRIMARY KEY (`id_trimestre`),
  KEY `fk_trimestre_suscripcion1_idx` (`suscripcion_id_suscripcion`),
  CONSTRAINT `fk_trimestre_suscripcion1` FOREIGN KEY (`suscripcion_id_suscripcion`) REFERENCES `suscripcion` (`id_suscripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trimestre`
--

LOCK TABLES `trimestre` WRITE;
/*!40000 ALTER TABLE `trimestre` DISABLE KEYS */;
INSERT INTO `trimestre` VALUES (1,'2022-03-16',1);
/*!40000 ALTER TABLE `trimestre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ubicacion`
--

DROP TABLE IF EXISTS `ubicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ubicacion` (
  `id_ubicacion` int NOT NULL AUTO_INCREMENT,
  `pais` varchar(100) NOT NULL,
  `id_ciudad_fk` int NOT NULL,
  PRIMARY KEY (`id_ubicacion`),
  KEY `fk_ubicacion_ciudad1_idx` (`id_ciudad_fk`),
  CONSTRAINT `fk_ubicacion_ciudad1` FOREIGN KEY (`id_ciudad_fk`) REFERENCES `ciudad` (`id_ciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ubicacion`
--

LOCK TABLES `ubicacion` WRITE;
/*!40000 ALTER TABLE `ubicacion` DISABLE KEYS */;
INSERT INTO `ubicacion` VALUES (1,'mexico',1),(2,'japon',2),(3,'aregentina',3),(4,'bracil',4),(5,'reino unido',5);
/*!40000 ALTER TABLE `ubicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vinculacion`
--

DROP TABLE IF EXISTS `vinculacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vinculacion` (
  `id_vinculacion` int NOT NULL AUTO_INCREMENT,
  `nom_vinculo` varchar(100) NOT NULL,
  `contraseña` varchar(100) NOT NULL,
  PRIMARY KEY (`id_vinculacion`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vinculacion`
--

LOCK TABLES `vinculacion` WRITE;
/*!40000 ALTER TABLE `vinculacion` DISABLE KEYS */;
INSERT INTO `vinculacion` VALUES (1,'usuario_1','***********'),(2,'usuario_2','*****'),(3,'usuario_3','***********'),(4,'usuario_4','**************'),(5,'usuario_5','**************'),(6,'usuario_6','*************'),(7,'usuario_7','*************'),(8,'usuario_8','***'),(9,'usuario_9','***********'),(10,'usuario_10','************'),(11,'usuario_11','***********'),(12,'usuario_12','**************'),(13,'usuario_13','**************'),(14,'usuario_14','*************'),(15,'usuario_15','**************');
/*!40000 ALTER TABLE `vinculacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vw_calse`
--

DROP TABLE IF EXISTS `vw_calse`;
/*!50001 DROP VIEW IF EXISTS `vw_calse`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_calse` AS SELECT 
 1 AS `id_clase`,
 1 AS `nombre`,
 1 AS `credito`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_comprobate`
--

DROP TABLE IF EXISTS `vw_comprobate`;
/*!50001 DROP VIEW IF EXISTS `vw_comprobate`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_comprobate` AS SELECT 
 1 AS `comprobante_pagocol`,
 1 AS `id_comprobante_pago`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_coreo`
--

DROP TABLE IF EXISTS `vw_coreo`;
/*!50001 DROP VIEW IF EXISTS `vw_coreo`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_coreo` AS SELECT 
 1 AS `id_correo`,
 1 AS `correo`,
 1 AS `id_vinculacion_fk`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_dato_user`
--

DROP TABLE IF EXISTS `vw_dato_user`;
/*!50001 DROP VIEW IF EXISTS `vw_dato_user`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_dato_user` AS SELECT 
 1 AS `id_dato_user`,
 1 AS `nombre`,
 1 AS `apellido_pat`,
 1 AS `apellido_mat`,
 1 AS `edad`,
 1 AS `id_ubicacion_fk`,
 1 AS `id_inicio_sesion_fk`,
 1 AS `id_plataforma_fk`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_empleado`
--

DROP TABLE IF EXISTS `vw_empleado`;
/*!50001 DROP VIEW IF EXISTS `vw_empleado`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_empleado` AS SELECT 
 1 AS `id_empleado`,
 1 AS `tipo`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'curso_linea'
--

--
-- Dumping routines for database 'curso_linea'
--
/*!50003 DROP FUNCTION IF EXISTS `funcion_uno` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `funcion_uno`(uno integer) RETURNS int
BEGIN 
RETURN uno+5;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `cual_pataforma` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cual_pataforma`(asociada varchar(45))
begin
	select plataforma.nom_plataforma as nombre_pataforma, dato_user.nombre as nombre_usario
    from plataforma
    inner join dato_user on id_plataforma_fk = plataforma.id_plataforma
    where plataforma.nom_plataforma = asociada;
    end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `num_2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `num_2`(in _id_dato_user int)
begin 
select * from curso_linea.dato_user,curso_linea.alumno
where dato_user.id_dato_user = _id_dato_user and dato_user.id_dato_user = alumno.id_dato_user_fk ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `vw_calse`
--

/*!50001 DROP VIEW IF EXISTS `vw_calse`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_calse` AS select `t1`.`id_clase` AS `id_clase`,`t1`.`nombre` AS `nombre`,`t1`.`credito` AS `credito` from `clase` `t1` where (`t1`.`nombre` = 'java de 0 a 100') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_comprobate`
--

/*!50001 DROP VIEW IF EXISTS `vw_comprobate`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_comprobate` AS select `t1`.`comprobante_pagocol` AS `comprobante_pagocol`,`t1`.`id_comprobante_pago` AS `id_comprobante_pago` from `comprobante_pago` `t1` where (`t1`.`comprobante_pagocol` = 'gracias por la compra: semestra') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_coreo`
--

/*!50001 DROP VIEW IF EXISTS `vw_coreo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_coreo` AS select `t1`.`id_correo` AS `id_correo`,`t1`.`correo` AS `correo`,`t1`.`id_vinculacion_fk` AS `id_vinculacion_fk` from `correo` `t1` where (`t1`.`correo` = 'njaskowicz0@w3.org') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_dato_user`
--

/*!50001 DROP VIEW IF EXISTS `vw_dato_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_dato_user` AS select `t1`.`id_dato_user` AS `id_dato_user`,`t1`.`nombre` AS `nombre`,`t1`.`apellido_pat` AS `apellido_pat`,`t1`.`apellido_mat` AS `apellido_mat`,`t1`.`edad` AS `edad`,`t1`.`id_ubicacion_fk` AS `id_ubicacion_fk`,`t1`.`id_inicio_sesion_fk` AS `id_inicio_sesion_fk`,`t1`.`id_plataforma_fk` AS `id_plataforma_fk` from `dato_user` `t1` where (`t1`.`edad` = '18') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_empleado`
--

/*!50001 DROP VIEW IF EXISTS `vw_empleado`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_empleado` AS select `t1`.`id_empleado` AS `id_empleado`,`t1`.`tipo` AS `tipo` from `empleado` `t1` where (`t1`.`tipo` = 'maestro') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-25 16:03:04
