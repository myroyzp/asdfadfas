import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Catalogo {
    private static ArrayList<Producto> productos = new ArrayList<Producto>();

    public static void generarStock() {
        Pan pan_1 = new Pan("M111", "Pande de muerto", "Pan de dulse con azucar", 10, true);
        Pan pan_2 = new Pan("M112", "Concha", "Pan de dulce con en parte de arriba", 6, true);
        Pan pan_3 = new Pan("M113", "Dona", "Pan de dulce bañado con chocolate", 10, true);
        Pan pan_4 = new Pan("M114", "bollilo", "pan salado", 2, true);
        Pan pan_5 = new Pan("M115", "Barrita", "Pan de dulce con relleno de sabor", 6, true);
        Producto.ProductosPan.add(pan_1);
        Producto.ProductosPan.add(pan_2);
        Producto.ProductosPan.add(pan_3);
        Producto.ProductosPan.add(pan_4);
        Producto.ProductosPan.add(pan_5);



        Pastel Pas_1 = new Pastel("M101", "Pastel de chocolate", 250, 1, true);
        Pastel Pas_2 = new Pastel("M102", "Pastel de Futas", 250, 1, true);
        Pastel Pas_3 = new Pastel("M103", "Paste de bodas sabor vainilla", 1000, 3, true);
        Pastel Pas_4 = new Pastel("M104", "Chiskey de frutas", 150, 1, true);
        Pastel Pas_5 = new Pastel("M105", "Chiskey de limon", 150, 1, true);
        Producto.ProductosPastel.add(Pas_1);
        Producto.ProductosPastel.add(Pas_2);
        Producto.ProductosPastel.add(Pas_3);
        Producto.ProductosPastel.add(Pas_4);
        Producto.ProductosPastel.add(Pas_5);
    }

    public static void anadirPan() {

        String Id;
        String Nombre;
        int Precio;
        String Caracter;
        boolean dispo = false;
        int ban=0;
        String tal;
        Scanner Teclado_Ca = new Scanner(System.in);
        Scanner Teclado_in = new Scanner(System.in);

        System.out.println("Ingresa un (ID) Para un nuevo Producto Pan");
        Id=Teclado_Ca.nextLine();

        System.out.println("Ingresa un (Nombre) Para un nuevo Producto Pan");
        Nombre=Teclado_Ca.nextLine();

        System.out.println("Ingresa una (Caracteristica) en especial que tendra el nuevo Producto Pan");
        Caracter=Teclado_Ca.nextLine();

        System.out.println("Ingresa un (Precio) para un nuevo Producto Pan");
        Precio=Teclado_in.nextInt();

        System.out.println("Hay disponibilidad del Producto?");
        System.out.println("Tienes productos en el stock de pan?" +
                "\nSi tienes escribe s/n de lo contrario");

        tal=Teclado_Ca.nextLine();

        if (tal.toLowerCase().equals("s".toLowerCase()))
        {
            System.out.println("Se agrego el producto como disponible");
            dispo=true;
        }else
        {
            System.out.println("Se agrego el producto como no lo disponible");
            dispo=false;
        }

        Producto nuevo = new Pan(Id,Nombre,Caracter,Precio,dispo);
        productos.add(nuevo);


    }

    public static void anaidirPastel(){
        String Id;
        String Nombre;
        int Precio;
        int Pisos ;
        boolean dispo = false;

        String tal;

        Scanner Teclado_Ca = new Scanner(System.in);
        Scanner Teclado_In = new Scanner(System.in);

        System.out.println("Ingresa un (ID) Para un nuevo Producto Pastel");
        Id = Teclado_Ca.nextLine();

        System.out.println("ingresa un (Nombre) Para un nuevo Producto Pastel");
        Nombre = Teclado_Ca.nextLine();

        System.out.println("ingresa un (Precio) Para un nuevo Producto Pastel");
        Precio = Teclado_In.nextInt();

        System.out.println("ingresa el numero de pisos (Pisos) que tendra el nuevo Pastel");
        Pisos = Teclado_In.nextInt();
        System.out.println("hay Pasteles disponibles?");

        System.out.println("Tienes productos en el stock de pastel?" +
                "\nsi tienes escribe s/n de lo contrario");
        tal = Teclado_Ca.nextLine();

        if(tal.toLowerCase().equals("s".toLowerCase())) {
            System.out.println("Se agrego este Producto agrego como disponible");
            dispo = true;

        } else {
            System.out.println("Se agrego este Producto como no disponible");
            dispo = false;
        }
        Producto nuevo = new Pastel(Id, Nombre, Precio, Pisos, dispo);
        productos.add(nuevo);

    }
    public static void lista(){

        if(productos.size()==0){
            System.out.println("No tienes ningun Producto en tu lista intentalo mas tarde");
        }else {
            System.out.println("\nTienes los siguintes Producto en tu lista\n");
            for (Producto p:productos){
                System.out.println(p.toString());
            }
        }
    }

    public static void Eliminar(){

        int opc;
        Scanner Teclado_In =new Scanner(System.in);

        do {
            System.out.println("Que quieres eliminar?\n" +
                    "Un solo producto o la lista completa de productos?\n" +
                    "1). Borrar toda la listas \n" +
                    "2). Borrar solo un producto\n" +
                    "3). Salir sin modificar");
            opc=Teclado_In.nextInt();
            switch (opc){
                case 1:
                    vaciar();
                    break;
                case 2:
                    soloUno();
                    break;
                case 3:
                    Principal.salida();
                    break;
                default:
                    Principal.defaul();
                    break;

            }
        }while (opc!=3);
    }
    public static void vaciar() {
        Scanner Teclado_Ca = new Scanner(System.in);
        System.out.println("Si esta seguro de borra la lista pon (s/n) de lo contrario");
        String opc = null;
        opc = String.valueOf(Teclado_Ca.nextInt());
        try {


        if (opc.toLowerCase().equals("s".toLowerCase())) {
            productos.clear();
            System.out.println("Se borro con exito");
        } else if (opc.toLowerCase().equals("n".toLowerCase())) {
            System.out.println("No se borro");
            lista();
        }
            }catch(Exception e){
            System.out.println("error metiste una tecla incprecta");
            vaciar();
        }
    }
    public static void soloUno(){
        lista();
        Scanner Teclado_Ca = new Scanner(System.in);

        System.out.println("Digite la matricula que quiere borrar");
        String opc = Teclado_Ca.nextLine();

        for (Producto p:productos){
            if (opc.toLowerCase().equals(p.getId().toLowerCase())){
                System.out.println("has elimiado el producto");
                productos.remove(p);
                break;
            }else{
                System.out.println("No se encuntra el producto que quieres borrar");
            }

        }
    }
}
