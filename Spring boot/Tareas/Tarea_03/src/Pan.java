public class Pan extends Producto implements StokcDispo {
    String Caracter;

    public Pan(String Id,String Nombre, String Caracter,int Precio, boolean Stock) {
        super(Id,Nombre, Precio, Stock);
        this.Caracter = Caracter;
    }

    public void setCaracter(String caracter) {
        Caracter = caracter;
    }

    public String getCaracter() {return Caracter;}

    @Override
    public String toString(){return
            "\nId del producto pan: "+getId()+
                    "\nNombre: "+getNombre()+
                    "\nCaracteristica: "+Caracter+
                    "\nPrecio: "+getPrecio()+
                    "\nProdructo a la venta: "+StokcDispo.getStockDispo(stock);
    }
}