import java.util.ArrayList;

public abstract  class Producto {
    static ArrayList<Producto> ProductosPan = new ArrayList<Producto>();
    static ArrayList<Producto> ProductosPastel = new ArrayList<Producto>();

    protected String Id;
    protected int Precio;
    protected String Nombre;
    protected boolean stock;


    public Producto(String Id, String Nombre, int Precio, boolean stock) {
        this.Nombre = Nombre;
        this.Precio = Precio;
        this.stock = stock;
        this.Id = Id;
    }

    public Producto() {

    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    //esto sirve  visualiza los datos de precio
    public float getPrecio() {
        return Precio;
    }

    //este sirve para visualizar los datos de nombre
    public String getNombre() {
        return Nombre;
    }

    //este sirve para visualizar los datos de stock
    public boolean getStock() {
        return stock;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public void setPrecio(int precio) {
        Precio = precio;
    }

    public void setStock(boolean stock) {
        this.stock = stock;
    }

    static void listaProductoDispo(){
        if (ProductosPan.size()==0){
            System.out.println("No has añadido los productos Predeterminados");

        }else {
            System.out.println("Tienes este catalago de pan Predeterminados");
            for (Producto p : ProductosPan) {
                System.out.println(p.toString());
            }
        }
    }

    static void listaProcutoDispo2(){
        if (ProductosPastel.size()==0){
            System.out.println("No has añadido los productos Predeterminados\n");
        }else {
            System.out.println("\nTenemos este catalgo de Pastel Predeterminados\n");
            for (Producto ps : ProductosPastel) {
                System.out.println(ps.toString());
            }
        }
    }

    public abstract String toString();
}
