public class Pastel extends  Producto{
    int Pisos;

    public Pastel(String Id, String Nombre, int Precio,int Pisos, boolean Stock) {
        super(Id,Nombre, Precio, Stock);
        this.Pisos=Pisos;
    }

    public int getPisos() {
        return Pisos;
    }

    @Override
    public String toString(){
        return   "\nId de producto del pastel: "+getId()+
                "\nNombre: "+getNombre()+
                "\nPiso: "+Pisos+
                "\nPrecio: "+getPrecio()+
                "\nProdructo a la venta: "+ StokcDispo.getStockDispo(stock);
    }
}
