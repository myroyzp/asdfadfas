package org.sisoftware;

public interface Shape {
    int computeArea();
}
