package org.sisoftware.command;

//Se crea esta nueva clase para hacer el tratamiento del pedido internacional, que se va a extender a la interface TratamientoPedido
public class TratamientoPedidoInternacional implements TratamientoPedido{

    private PedidoInternacional pedidoInternacional;

    public TratamientoPedidoInternacional(PedidoInternacional pedidoInternacional){
        this.pedidoInternacional = pedidoInternacional;
    }

    //Para sobreescribir la función en esta clase
    @Override
    public boolean tratar(){
        return "Mordor".equalsIgnoreCase(pedidoInternacional.getDestino()) ? false : true;
    }
}