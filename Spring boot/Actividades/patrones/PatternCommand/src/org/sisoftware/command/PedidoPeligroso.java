package org.sisoftware.command;

public interface PedidoPeligroso extends Pedido {

     String instrucciones();

}
