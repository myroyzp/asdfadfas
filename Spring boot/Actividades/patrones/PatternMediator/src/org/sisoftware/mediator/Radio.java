package org.sisoftware.mediator;

public class Radio {

	private boolean encendida = false;
	private Evento evento;
	
	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public void enciende() {
		encendida = true; 
		evento.enciendeRadio();
	}
	
	public void apaga() {
		encendida = false;
		evento.apagaRadio();
	}
	
	public boolean encendida() {
		return encendida;
	}
}
