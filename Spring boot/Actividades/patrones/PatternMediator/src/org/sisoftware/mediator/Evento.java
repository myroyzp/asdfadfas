package org.sisoftware.mediator;


//Se crea esta clase para que Mediar a las demás
public class Evento{
    private Telefono telefono;
    private Coche coche;
    private Radio radio;

    public Evento (Telefono telefono, Coche coche, Radio radio) {
        this.telefono = telefono;
        this.coche = coche;
        this.radio = radio;
        this.telefono.setEvento(this);
        this.coche.setEvento(this);
        this.radio.setEvento(this);
    }
    //Si se enciende el coche, la radio se enciende y el teléfono apaga la música
    public void enciendeCoche(){
        radio.enciende();
        telefono.apagaMusica();
    }
    //Si el coche se apaga, la radio se apaga
    public void apagaCoche(){
        radio.apaga();
    }
    //Si se enciende la radio, la música se apaga
    public void enciendeRadio(){
        telefono.apagaMusica();
    }
    //Si se recibe una llamada, la radio se apaga
    public void recibeLlamada(){
        radio.apaga();
    }

    public void apagaRadio() {
    }
}
