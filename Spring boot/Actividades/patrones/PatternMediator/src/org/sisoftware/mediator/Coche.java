package org.sisoftware.mediator;

public class Coche {

	private Evento evento;
	
	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public void enciende(){evento.enciendeCoche();}
	public void apaga(){evento.apagaCoche();}
}
