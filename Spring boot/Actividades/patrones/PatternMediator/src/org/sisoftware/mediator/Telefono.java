package org.sisoftware.mediator;

public class Telefono {

	private boolean musicaOn = false;
	private Evento evento;
	
	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public void recibeLlamada() {
		evento.recibeLlamada();
	}

	public void enciendeMusica() {
		musicaOn = true;
	}
	
	public void apagaMusica() {
		musicaOn = false;
	}
	
	public boolean musicaEncendida() {
		return musicaOn;
	}
}
