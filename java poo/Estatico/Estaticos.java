package Estatico;

public class Estaticos {
    //estatico atributo
    static String atributo = "valor 1";
    //Estatico atributo
    static String atributoEstatico = "valor 1";

    public static void main(String[] args) {
        //objeto 1
        Estaticos obj1 = new Estaticos();
        //objeto 2
        Estaticos obj2 = new Estaticos();
        //imprimir valores

        System.out.println(obj1.atributoEstatico);
        System.out.println(obj2.atributoEstatico);

        //ejemplo in -- correcto ek atributo se debe llamr desde la clase no en los objetos
        Estaticos.atributoEstatico="valor3";
        System.out.println( Estaticos.atributoEstatico);
    }
}
