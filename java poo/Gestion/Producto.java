package Gestion;

import java.util.ArrayList;

public class Producto {

    static ArrayList<Producto> productosDisponibles = new ArrayList<Producto>();
    private String sku, nombre, descripcion;
    private double precio;
    private int cantidad;

    public Producto(){
    }

    public Producto(String sku,String nombre,double precio,String descripcion, int cantidad){
        this.sku = sku;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
    }

    void establecerSKU(String x){ sku = x; }
    void establecerNombre(String x) { nombre = x; }
    void establecerPrecio( double x) { precio = x; }
    void establecerDescripcion(String x){ descripcion = x; }
    void establecerCantidad(int x){ cantidad = x; }

    String obtenerSKU(){ return sku; }
    String obtenerNombre() { return nombre;}
    double obtenerPrecio() { return precio; }
    String obtenerDescripcion(){ return descripcion; }
    int obtenerCantidad(){ return cantidad; }

    static void listarProductosDisponibles(){
        System.out.println("" +
                "--------------------------\n" +
                "Tenemos los siguientes productos:\n" +
                "---------------------------------\n"
        );
        for(Producto producto: productosDisponibles){
            System.out.println("" +
                    producto.obtenerSKU() +" - " +  producto.obtenerNombre() +": "+ producto.obtenerPrecio()
            );
        }
        System.out.println("" +
                "--------------------------\n\n\n"
        );
    }

}
