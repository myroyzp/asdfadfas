package Gestion;

import java.util.ArrayList;
import java.util.Locale;

import Entrada.Teclado;

public class Carrito {

    private static ArrayList<Producto> productos = new ArrayList<Producto>();
    private static double total;

    public static void anadir(){
        //saber que producto añadir
        Producto.listarProductosDisponibles();
        System.out.println("¿que productos desea añdir?");
        String seleccion = Teclado.LeeCadena();
        //preguntar cuantos productos desea añadri
        System.out.println("cuantos productos desae añadir");
        int cantidad = Teclado.LeeEntero();
        for(Producto p:Producto.productosDisponibles){
            if(seleccion.toLowerCase().equals(p.obtenerSKU().toLowerCase())){
                //hacer un copia antes de añadir
                Producto copia = new Producto(p.obtenerSKU(), p.obtenerNombre(),p.obtenerPrecio(),p.obtenerDescripcion(),cantidad);
                productos.add(copia);//poe que copia por que se adrean una nueva cantidad a dicho caracter
                System.out.println("se añadio el producto exitosamente");

            }

        }
    }

    public static void listar(){
        if(productos.size()==0){
            System.out.println("tu carrito esta vacio");
        }else {
            total=0;
            System.out.println("este es tu carrioto de compras");
            for (Producto p : productos) {

                System.out.println(p.obtenerSKU() + "-" + p.obtenerNombre() + "$" + p.obtenerPrecio() + " c/u " + p.obtenerCantidad());
                total += p.obtenerPrecio()*p.obtenerCantidad();
            }
        System.out.println("El total a pagar es de: $"+total);
        }
    }

    public static void cambiarCantidadProducto(){
        listar();
        System.out.println("Escribe el sku del producto que deseas modifcar");
        String seleccion = Teclado.LeeCadena();
        System.out.println("cual sera la nueva cantidad del producto que deseas modificar"+seleccion+"?");
        int nuevacantidad = Teclado.LeeEntero();
        for(Producto p :productos){
            if(seleccion.toLowerCase().equals( p.obtenerSKU().toLowerCase())){
                //si el sku esta en el areglo
                p.establecerCantidad(nuevacantidad);//cambiar por una nueva cantidad
                System.out.println("la cantidad fue modiifcada exitosamente");
                listar();
            }

        }

    }

    public static void eliminarProducto(){
        listar();
        System.out.println("Escribe el Sku del producto a eliminar");
        String seleccion = Teclado.LeeCadena();
        for (Producto p:productos){
            if(seleccion.toLowerCase().equals(p.obtenerSKU().toLowerCase())){
                productos.remove(p);
                System.out.println("Fue eliminado exitosamente");
                break;
            }
        }
        listar();
    }

    public static void vaciar() {
        if (productos.size() > 0) {

            System.out.println("Realmente deseas vaciar tu carrito s/n");
            String seleccion = Teclado.LeeCadena();

            if (seleccion.toLowerCase().equals("s")) {
                productos.clear();
                listar();

            } else {
                System.out.println("el carrito no se vacio");
            }

        }else{
            System.out.println("tu carrito de compras ya esta vacio");
        }

    }
}