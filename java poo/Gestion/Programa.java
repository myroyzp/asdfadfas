package Gestion;

import java.util.ArrayList;
import Entrada.*;

public class Programa {

    public static void main(String[] args) {

        generarInventario();
        iniciarPrograma();

    }


    public static void generarInventario(){

        //Generar productos disponibles
        Producto producto1 = new Producto("A001", "Leche Lala 1L", 20, "Leche pura de vaca", 10);
        Producto producto2 = new Producto("A002", "Agua Bonafont 1L", 10, "Agua pura de manantial 1L", 10);
        Producto producto3 = new Producto("A003", "Agua Bonafont 1.5L", 12, "Agua pura de manantial 1.5L", 10);
        Producto producto4 = new Producto("A004", "Agua Bonafont 2L", 15, "Agua pura de manantial 2L", 10);
        Producto producto5 = new Producto("A005", "Agua Bonafont 600ml", 8, "Agua pura de manantial 600ml", 10);
        Producto producto6 = new Producto("A006", "Cereal Kellog's Corn Flakes", 40, "Cereal para toda la familia", 10);
        Producto producto7 = new Producto("A007", "Cereal Kellog's Choco Crispis", 43, "Cereal para toda la familia sabor chocolate", 10);
        Producto producto8 = new Producto("A008", "Cereal Kellog's Zucaritas", 43, "Cereal para toda la familia azucarado", 10);
        Producto producto9 = new Producto("A009", "Vasos de plático (10)", 11, "Vasos para fiesta, deshechables.", 10);
        Producto producto10 = new Producto("A010", "Platos de plástico (10)", 14, "Platos para fiesta, deshechables.", 10);

        //Añadirlos al listado de productos disponibles
        //Refactorizar para hacer un código menos suceptible a errores.
        Producto.productosDisponibles.add(producto1);
        Producto.productosDisponibles.add(producto2);
        Producto.productosDisponibles.add(producto3);
        Producto.productosDisponibles.add(producto4);
        Producto.productosDisponibles.add(producto5);
        Producto.productosDisponibles.add(producto6);
        Producto.productosDisponibles.add(producto7);
        Producto.productosDisponibles.add(producto8);
        Producto.productosDisponibles.add(producto9);
        Producto.productosDisponibles.add(producto10);


    }

    public static void iniciarPrograma(){
        int opc;


        do {
            System.out.println("Bienvenido/a a Amazon, ¿Qué deseas hacer?" +
                    "\n1. Ver listado de productos disponibles" +
                    "\n2. Añadir un producto al carrito" +
                    "\n3. Ver carrito de compras" +
                    "\n4. Cambiar cantidad de un producto en el carrito" +
                    "\n5. Eliminar producto de un carrito" +
                    "\n6. Vaciar carrito"+
                    "\n7. Salir");


            opc = Teclado.leerEntero();

            switch(opc){

                case 1:
                    Producto.listarProductosDisponibles();
                    esperar();
                    break;
                case 2:
                    Carrito.anadir();
                    esperar();
                    break;
                case 3:
                    Carrito.listar();
                    esperar();
                    break;
                case 4:
                    Carrito.cambiarCantidadProducto();
                    esperar();
                    break;
                case 5:
                    Carrito.eliminarProducto();
                    esperar();
                    break;
                case 6:
                    Carrito.vaciar();
                    esperar();
                    break;
                case 7:
                    salir();
                    break;
                default:
                    errorMenu();
                    break;


            }



        }while(opc != 7);
    }


    private static void salir(){
        System.out.println("Gracias por visitarnos. Vuelve pronto.");
    }

    private static void errorMenu(){
        System.out.println("Ups, no encontramos esa opción en el menú. Intenta de nuevo\n\n\n");
    }

    private static void esperar(){
        System.out.println("Oprime Enter para continuar.");
        String e = Teclado.leerCadena();
    }



}

