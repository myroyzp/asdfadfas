package Herencia;

public class Persona {
    private String nombre, apellido;
    private int edad;
    protected String telefono;

    public Persona(String nombre, String apellido, int edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public int getEdad(){
        return edad;
    }

    //generar un metodo que me imprima todos loa datos
    //de esta clase
    public void mostraDatos(){
        System.out.println("Nombre:\t"+getNombre());
        System.out.println("apellido:\t"+getApellido());
        System.out.println("edad:\t"+getEdad());
    }
}
