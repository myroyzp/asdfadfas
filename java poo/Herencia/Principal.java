package Herencia;

import Herencia.*;

public class Principal {
    public static void main(String[] args) {

        //generar una persona
        Persona obj1=new Persona("Rodrigo","Zamudio",22);

        //generar un Estudiante

        Estudiante obj2 = new Estudiante("harry","potter",25,123,8.0f);

        //Imprimir datos persona
        obj1.mostraDatos();
        //imprimir datos Estudiante
        obj2.mostradatos();
        //DEFAULT
        obj2.salon="2b";
        obj2.asistencia="no asiste";
    }
}

