package Herencia;

public class Estudiante extends  Persona{


    private int codigoEstudiante;
    private float notaFinal;
     String salon = "3B";
     String asistencia = "asiste";



    public Estudiante(String nombre, String apellido, int edad,int codigoEstudiante,float notaFinal) {
        super(nombre, apellido, edad);
        this.codigoEstudiante=codigoEstudiante;
        this.notaFinal=notaFinal;
    }
    public void mostradatos(){

        System.out.println("Nombre:\t" + getNombre());
        System.out.println("Apellido:\t " + getApellido());
        System.out.println("Edad:\t " + getEdad());
        System.out.println("codigo:\t"+codigoEstudiante);
        System.out.println("nota final:\t"+notaFinal);
    }


}
