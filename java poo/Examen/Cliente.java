package Examen;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Locale;

import Entrada.Teclado;
public class Cliente {
    public static ArrayList<Agenda> misclientes = new ArrayList<Agenda>();

    public static void Añadir(){
        System.out.println("digite la matricula del alumno unicamente valores enteros");
        int matricula = Teclado.LeeEntero();
        System.out.println("digite el nombre ");
        String nombre = Teclado.LeeCadena();

        System.out.println("digite el telefono ");
        String telefono = Teclado.LeeCadena();
        System.out.println("digite el correo ");
        String correo = Teclado.LeeCadena();

        Agenda nuevo = new Agenda(matricula,nombre,telefono,correo);
        misclientes.add(nuevo);
        System.out.println("se actualiso tu agenda");

    }

    public static void listar(){
        if (misclientes.size()==0){
            System.out.println("\n\n\nlos nuevos clientes se encuentra vacios\n\n\n");
        }
        else {
            System.out.println("\n\n\ntienes los siguientes agenda de clientes\n\n\n ");
            for (Agenda p:misclientes){
                System.out.println(p.obtenermatricula()+" Nombr. "+p.obtenernombre()+" # "+p.obtenertelefono()+" @ "+p.obtenercorreo());

            }
            System.out.println("\n\n\n");
        }
    }
    public static void eliminar(){
        listar();

        System.out.println("\n\ndigite la matricula que dece eliminar\n\n");
        Integer seleccion = Teclado.leerEntero();

        for (Agenda p:misclientes){
            if (seleccion.equals(p.obtenermatricula())){
                System.out.println("has eliminado la matricula de la agenda\n\n");
                misclientes.remove(p);
                break;
            }
        }
    }
    public static void vaciar(){
        System.out.println("esta seguro que deceas vaciar la agenda?");
        System.out.println("digita la tecla s para confirmar");

        String seleccion = Teclado.LeeCadena();
        if (seleccion.toLowerCase().equals("s")){
            misclientes.clear();
            System.out.println("se a vaciado el registro seleccionado\n\n\n");

        }
        else {
            System.out.println("registros vacios");
            listar();
        }
    }
    public static void modificar(){

        listar();
        int opc;

        do {
            System.out.println("\npreciona (0.)si quieres ver a tus clientes");
            System.out.println("\npreciona (1.)si decesa modificar la matricula");
            System.out.println("\npreciona (2.)si decesa modificar el nombre");
            System.out.println("\npreciona (3.)si decesa modificar la telefono");
            System.out.println("\npreciona (4.)si decesa modificar la correo");
            System.out.println("\nprecione (5.)si deceas salir del menu");

            opc=Teclado.LeeEntero();

            switch (opc){
                case 0:
                    listar();
                break;
                case 1:

                    modificarMatricula();
                break;
                case 2:
                    modifignombre();
                break;
                case 3:
                    modifigTelefo();
                break;
                case 4:
                modifigCorreo();
                break;
                case 5:
                    salida();
                 break;
                default:
                    error();
                 break;
            }
        }while (opc !=5);
        System.out.println("\n\n\n");
    }

    public static void modificarMatricula(){

        System.out.println("digita la matricula que deceas buscar");
        Integer num = Teclado.LeeEntero();
        for (Agenda p:misclientes){
            if (num.equals(p.obtenermatricula())){
                System.out.println("menu para modificar la matricula");
                System.out.println("digita el numero de la matricula que deceas modificar");
                int nuevo_num = Teclado.LeeEntero();
                p.estableceMatricula(nuevo_num);
                System.out.println("SE MODIFICO EXITOSAMNETE");

                break;
            }
            else{
                System.out.println("ups se selecciono un cliente inexistente");
                Programa.iniciodeProgram();
            }
        }
    }

    public static void modifignombre(){
        System.out.println("digite la matricula");
        Integer num  =Teclado.LeeEntero();
        for (Agenda p: misclientes){
            if(num.equals(p.obtenermatricula()))
            {
                System.out.println("MENU PARA MODIFICAR NOMBRE");
                System.out.println("digite el nuevo nombre que de le dara al cliente");
                String nuevo_nom = Teclado.LeeCadena();
                p.establecenombre(nuevo_nom);
                System.out.println("SE MODIFICO EXITOSAMNETE");
                break;
            }
            else{
                System.out.println("ups se selecciono un cliente inexistente");
                Programa.iniciodeProgram();
            }
        }
    }

    public static void modifigTelefo(){
        System.out.println("digitr la matricula");
        Integer num = Teclado.LeeEntero();
        for (Agenda p :misclientes){
            if (num.equals(p.obtenermatricula()))
            {
                System.out.println("MENU PARA MODIFICAR TELEFONO");
                System.out.println("DIGITE EL NUEVO TELEFONO DEL CLIENTE");
                String nuevo_tele = Teclado.LeeCadena();
                p.establecetelegono(nuevo_tele);
                System.out.println("SE MODIFICO EXITOSAMNETE");
                break;
            }
            else{
                System.out.println("ups se selecciono un cliente inexistente");
                Programa.iniciodeProgram();
            }
        }
    }

    public static void modifigCorreo(){
        System.out.println("digite la matricual");
        Integer num = Teclado.LeeEntero();
        for (Agenda p : misclientes){
            if (num.equals(p.obtenermatricula())){
                System.out.println("MENU DE MODIFICAR CORREO");
                System.out.println("DEGITE EL NUEVO CORREO DEL CLIETE");
                String nuevo_correo = Teclado.LeeCadena();
                p.establececorreo(nuevo_correo);
                System.out.println("SE MODIFICO EXITOSAMNETE");
                break;

            }
            else{
                System.out.println("ups se selecciono un cliente inexistente");
                Programa.iniciodeProgram();
            }
        }
    }
    public static void salida(){
        System.out.println("has salido del menu modificar\n\n\n");
    }
    public static void error(){
        System.out.println("\n\n\n PRESIONA UNICAMETE LOS ACCIONES INDACADAS \n\n\n");
    }
}