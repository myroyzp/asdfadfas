package Examen;

import Entrada.Teclado;
public class Programa {
    public static void main(String[] args) {

        iniciodeProgram();

    }

    public static void iniciodeProgram(){
        int opc;
        do{
            System.out.println("Bienvenido a la agenda de clientes, ¿Qué deseas hacer?" +
                    "\n1. Ver listado de clientes" +
                    "\n2. Añadir un cliente a la agenda" +
                    "\n3. modificar datos de un cliente" +
                    "\n4. eliminar un cliente" +
                    "\n5. vaciar agenda" +
                    "\n6. salir");
            opc = Teclado.LeeEntero();
            switch (opc){
                case 1:
                    Cliente.listar();
                 break;

                case 2:
                    Cliente.Añadir();
                break;

                case 3:
                   Cliente.modificar();
                break;

                case 4:
                        Cliente.eliminar();
                break;

                case 5:
                    Cliente.vaciar();
                break;

                case 6:
                    salir();
                break;

                default:
                    errorMenu();
                break;
            }
        }while(opc!=6);
    }
    private static void salir(){
        System.out.println("Has cerrado la agenda de clients hasta luego");
    }
    private static void errorMenu(){
        System.out.println("Error no se encuetra lo que esta marcando \nsolo selecciona las opcines que aparecen");
    }
}
