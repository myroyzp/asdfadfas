package Poliformismo;

public class Vehiculo {
    
    String Matricula;
    String Marca;
    String Modelo;
    
    public Vehiculo(String Matricula, String Marca, String Modelo){
        this.Matricula = Matricula;
        this.Marca = Marca;
        this.Modelo=Modelo;
    }
    
    public String getMatricula()
    {return Matricula;}
    
    public String getMarca()
    {return Marca;}
    
    public String getModelo()
    {return Modelo;}
    
    public void mostraDatos()
    {
        System.out.println("Mostra matricula:\t" + getMatricula());
        System.out.println("Mostras marca:\t"+getMarca());
        System.out.println("mostra modelo:\t"+getModelo());
    }            
}