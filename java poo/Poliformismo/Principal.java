package Poliformismo;

import Poliformismo.*;


public class Principal {

    public static void main(String[] args) {
        Vehiculo vehiculo=new Vehiculo("Matricula","Marca","Modelo");
        VehiculoDeportivo vehiculoDeportivo = new VehiculoDeportivo(475,"IN-GT116E","Audi","RS e-tron");
        VehiculoFurgoneta  vehiculoFurgoneta = new VehiculoFurgoneta(2780,"","Ford","Transit-2019");
        VehiculoTurismo vehiculoTurismo = new VehiculoTurismo(2,"VI2-AML","Aston Martin","DB11");
            
        carros(vehiculo);
        carros(vehiculoDeportivo);
        carros(vehiculoFurgoneta);
        carros(vehiculoTurismo);
    }
    
    public static void carros(Vehiculo vehiculo){
        vehiculo.mostraDatos();
                
    }
}
