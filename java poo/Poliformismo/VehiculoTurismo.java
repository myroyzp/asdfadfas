package Poliformismo;

public class VehiculoTurismo extends Vehiculo{
 int numerosPuerta;
    public VehiculoTurismo(int numeroPuerta,String Matricula,String Marca,String Modelo){
        super(Matricula,Marca,Modelo);
        this.numerosPuerta=numeroPuerta;
    }
    public void mostraDatos()
    {
        super.mostraDatos();
        System.out.println("numeroPuerta:\t " + numerosPuerta);
    }
}
