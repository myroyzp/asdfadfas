package Poliformismo;

public class VehiculoDeportivo extends Vehiculo{
    int cilindrada;
    
    public VehiculoDeportivo(int cilindrada, String Matricula, String Marca, String Modelo ){
    super(Matricula,Marca,Modelo);
    this.cilindrada=cilindrada;
    }
    public void mostrarDatos(){
        super.mostraDatos();
        System.out.println("Cileindrada:\t"+cilindrada);
    }
}
