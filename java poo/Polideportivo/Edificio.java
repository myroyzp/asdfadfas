package Polideportivo;

public abstract class Edificio {
    
   private float Ancho;
   private float Largo;

   public Edificio(float Ancho, float Largo) {
      this.Ancho=Ancho;
      this.Largo=Largo;
   }

   public float getSuperdiciEdificio(){return Ancho * Largo;}
   public abstract String toString();
}
