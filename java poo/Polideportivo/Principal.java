package Polideportivo;
import java.util.ArrayList;

public class Principal {

    public static void main(String[] args) {
        ArrayList<Edificio> edificios = new ArrayList<>();




        Edificio edificio1 = new EdificioOficinas(24,34,355,46);
        Edificio edificio2 = new EdificioOficinas(25,40,505,40);
        Edificio edificio3 = new EdificioOficinas(10,100,90,90);
        Edificio edificio4 = new EdificioOficinas(100,100,800,100);
        Edificio edificio5 = new EdificioOficinas(2,10,30,90);
        Polideportivo polideportivo1 = new Polideportivo(15,10,true);
        Polideportivo polideportivo2 = new Polideportivo(10,20,false) ;
        Polideportivo polideportivo3 = new Polideportivo(40,50,false);
        Polideportivo polideportivo4 = new Polideportivo(50,30,true);
        Polideportivo polideportivo5 = new Polideportivo(10,100,false);


        edificios.add(edificio1);
        edificios.add(edificio2);
        edificios.add(edificio3);
        edificios.add(edificio4);
        edificios.add(edificio5);
        edificios.add(polideportivo1);
        edificios.add(polideportivo2);
        edificios.add(polideportivo3);
        edificios.add(polideportivo4);
        edificios.add(polideportivo5);

        for(Edificio e :edificios ){

            System.out.println(e.toString());
        }
        //System.out.println(edificios);

    }
}
