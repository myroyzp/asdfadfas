package Polideportivo;

public class EdificioOficinas extends Edificio{
    
    int numeroPiso;
    int numeroOficina;

    public EdificioOficinas(int numeroPiso, int numeroOficina,float Ancho,float Largo) {
        super(Ancho,Largo);
        this.numeroPiso = numeroPiso;
        this.numeroOficina = numeroOficina;
    }

    
    public String toString(){
        
        return "\nNumero de Oficinas: "+numeroOficina
                +"\nNumero de Piso: "+ numeroPiso
                +"\nSuperficie: "+getSuperdiciEdificio();
    }
}
