package Polideportivo;

public interface InstalacioneDeportiva {

   static String getTipoinstalacion(boolean techado)
   {
       return techado? "Techado":"Abierto";
   }

}
