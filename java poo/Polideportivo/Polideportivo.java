package Polideportivo;

public class Polideportivo extends Edificio implements InstalacioneDeportiva{

    boolean tipos=true;

    public Polideportivo(float Ancho, float Largo,boolean tipos) {
        super(Ancho, Largo);
        this.tipos=tipos;
    }

    @Override
    public String toString(){
        return " \ntipo de estructura: "+InstalacioneDeportiva.getTipoinstalacion(tipos)
        +"\nSuperFicie: "+getSuperdiciEdificio();
    }


        
}
